// ES5 compatibility
window.addEventListener("load", function() {
  var d = new Date();
  var day = d.getDate();
  var month = d.getMonth() + 1;
  var isNewYears = day === 1 && month === 1 ? "Yes" : "No";
  var answer = '<div class="text">' + isNewYears + "</div>";
  document.getElementById("container").innerHTML = answer;
});
