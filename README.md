# README

A micro project, using simple old-school web tools, that is just a joke. "Is it new year's today?" The answer is revealed on the webpage.

## App Information

App Name: new-years

Created: Feburary 2020

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/new-years)

Production: [Link](https://is-it-new-years-today.netlify.app)

## Tech Stack

- HTML5
- CSS3
- JavaScript (ES5)

Last updated: 2024-11-01
